import datetime
import json

class Alert:
    def __init__(self, alert_model):
        self.data = alert_model
        self.timestamp = alert_model['@timestamp']
        self.id = alert_model['kibana.alert.uuid']
        self.post_id = -1
        self.is_open = self.data['kibana.alert.status'] == 'active'

    @staticmethod
    def alert_from_structured_data(alert_data):
        alert_data['alert'] = json.loads(alert_data['alert'])
        alert_data['context'] = json.loads(alert_data['context'])
        params = json.loads(alert_data['rule']['params'])
        alert_data['rule']['tags'] = alert_data['rule']['tags'].split(',')
        alert_data['details'] = Alert.remap_details_on_group(params['groupBy'], alert_data['kibana']['alert']['id'].split(','))
        alert_data['id'] = Alert.extract_id_from_url(alert_data['context']['alertDetailsUrl'])
        alert_data['context']['reason'] += '\n\n'
        for group, info in alert_data['details'].items():
            alert_data['context']['reason'] +=  '* ' + group + ': ' + info + '\n'
        if 'moreInfo' in alert_data:
            for key, info in alert_data['moreInfo'].items():
                alert_data['context']['reason'] +=  '* ' + key + ': ' + info + '\n'

        data = {
            '@timestamp': alert_data['@timestamp'],
            'kibana.alert.status': alert_data['kibana']['alert']['state'],
            'kibana.alert.uuid': Alert.extract_id_from_url(alert_data['context']['alertDetailsUrl']),
            'kibana.alert.start': alert_data['@timestamp'],
            'kibana.alert.reason': alert_data['context']['reason'],
            'kibana.alert.rule.name' :alert_data['rule']['name'],
            'kibana.alert.rule.tags': alert_data['rule']['tags'],
            'kibana.alert.rule.category': alert_data['rule']['type'],
            'kibana.alert.instance.id': alert_data['kibana']['alert']['id'],
            'event.kind': alert_data['event']['kind'],
            'alert.link': "https://monitor-kibana.socle.developpeurs.diplomatie.fr" + alert_data['context']['alertDetailsUrl'],
        }

        return Alert(data)

    @staticmethod
    def extract_id_from_url(url):
        return url.rsplit('/', 1)[-1]

    @staticmethod
    def dedupe_alerts(alertsA, alertsB):
        alerts = alertsA
        for searchedAlert in alertsB:
            found = False
            for existingAlert in alertsA:
                if searchedAlert.data['kibana.alert.uuid'] == existingAlert.data['kibana.alert.uuid']:
                    found = True
                    break
            if found == False:
                alerts.append(searchedAlert)

        return alerts

    @staticmethod
    def sort_alerts_by_timestamp(alerts):
        return sorted(alerts, key=lambda x: x.data['@timestamp'])

    @staticmethod
    def remap_details_on_group(groups, details):
        mappedDetails = {}
        for i, group in enumerate(groups):
            mappedDetails[group] = details[i]
        return mappedDetails

    @staticmethod
    def esiso2lstr(es_iso):
        return Alert.dt2lstr((Alert.esiso2datetime(es_iso)))

    @staticmethod
    def esiso2datetime(es_iso):
        # es datetimestr 2 datetime py
        return datetime.datetime.strptime(es_iso[:-1] + '000', '%Y-%m-%dT%H:%M:%S.%f')

    @staticmethod
    def dt2lstr(dt_in):
        # conversion timezone + formattage
        return (
            dt_in + dt_in.astimezone().utcoffset()
        ).strftime('%d/%m/%Y %H:%M:%S')

    def __str__(self):
        message_content = f"""
# {self.data['event.kind']} - {self.data['kibana.alert.rule.name']} : {self.data['kibana.alert.status']}

**Depuis**: {Alert.esiso2lstr(self.data['kibana.alert.start'])}
**Fin**: {Alert.esiso2lstr(self.data['kibana.alert.end']) if 'kibana.alert.end' in self.data else ""}

**id**: {self.id}
**tags**: {", ".join(self.data['kibana.alert.rule.tags'] + self.data.get('tags', []))}

**Categorie**: {self.data['kibana.alert.rule.category']}
**Instance.id**: {self.data['kibana.alert.instance.id']}

**More info**: { self.data['alert.link'] if 'alert.link' in self.data else ""}

{self.data.get('kibana.alert.reason', 'no reason mentioned')}

"""
        return message_content
