FROM python:slim-bookworm
RUN useradd elastic2mm -u 1000 -m
COPY elastic2mattermost /elastic2mattermost
USER elastic2mm
ENV PATH=/home/elastic2mm/.local/bin:$PATH
RUN python3 -m pip install -r elastic2mattermost/requirements.txt
ENTRYPOINT [ "/usr/local/bin/python" ]
CMD [ "/elastic2mattermost" ]