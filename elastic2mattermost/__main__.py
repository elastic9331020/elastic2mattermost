#!/bin/python3
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('main')
from elastic_alert import ElasticAlertExtractor
from mattermost import MattermostConnector
from alert import Alert
import os, json, datetime
from decouple import config
FIRST_START_MINUTES_DELTA=360 # 30 minutes
last_fetch_date_file_path = "/tmp/elastic2mattermost/lastfetchdate"
last_fetch_date = datetime.datetime.now() - datetime.timedelta(minutes=FIRST_START_MINUTES_DELTA)
if os.path.isfile(last_fetch_date_file_path):
    with open(last_fetch_date_file_path, "r") as last_fetch_date_file:
        last_fetch_date = datetime.datetime.fromtimestamp(float(last_fetch_date_file.read()))
logger.debug('Start, last_fetch_date=%s',last_fetch_date)

es = ElasticAlertExtractor(config('ES_HOSTS').split(','), config('ES_LOGIN'), config('ES_PASSWORD'), config('ES_ASSERT_FINGERPRINT'))
mtm = MattermostConnector(config('MTM_PROTO'), config('MTM_HOSTNAME'), config('MTM_PORT', cast=int), config('MTM_TOKEN'))

list_alert = es.get_alerts_from_internal_index(last_fetch_date)
logger.info('%i internal alerts found', len(list_alert))

list_alert_custom = es.get_alerts_from_forgecloud_custom_index(last_fetch_date)
logger.info('%i custom alerts found', len(list_alert_custom))

deduped_alerts = Alert.dedupe_alerts(list_alert_custom, list_alert)
logger.info('%i deduped alerts found', len(deduped_alerts))


for alert in Alert.sort_alerts_by_timestamp(deduped_alerts):
    logger.info('Notify Mattermost for Alert %s', alert.id)
    mtm.notify_alert(
        alert
    )

f = open(last_fetch_date_file_path, "w")
f.write(str(datetime.datetime.timestamp(datetime.datetime.now())))
f.close()
logger.debug('end')
