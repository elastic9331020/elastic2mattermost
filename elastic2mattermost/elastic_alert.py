import logging
logger = logging.getLogger('ElasticAlertExtractor')
from elasticsearch import Elasticsearch
from alert import Alert
import json
class ElasticAlertExtractor:
    def __init__(self, elasticsearch_urls, es_auth_user, es_auth_password, ssl_assert_fingerprint):
        print(elasticsearch_urls)
        self.es = Elasticsearch(
            hosts=elasticsearch_urls,
            basic_auth=(es_auth_user, es_auth_password),
            ssl_assert_fingerprint=(ssl_assert_fingerprint)
        )
        logger.debug('Connected to Elasticsearch')

    def get_alerts_from_internal_index(self, since_datetime):
        result = self.es.search(
            index=".internal.alerts-*",
            query = {
                "range": {
                  "@timestamp": {
                     "gte": since_datetime.strftime("%Y-%m-%dT%H:%M:%SZ")
                  }
                }
            },
            sort = [
             { "@timestamp" : "asc" }
            ]
        )
        logging.debug('INT get_alerts_from_internal_index result -> %s', json.dumps(result.body['hits']['hits']) )
        return [ Alert(alert['_source']) for alert in result.body['hits']['hits'] ]

    def get_alerts_from_forgecloud_custom_index(self, since_datetime):
        result = self.es.search(
            index="forgecloud-alerts",
            query = {
                "range": {
                  "@timestamp": {
                    "gte": since_datetime.strftime("%Y-%m-%dT%H:%M:%SZ")
                  }
                }
            },
            sort = [
             { "@timestamp" : "asc" }
            ]
        )
        logging.debug('CUSTOM get_alerts_from_forgecloud_custom_index result -> %s', json.dumps(result.body['hits']['hits']) )
        return [ Alert.alert_from_structured_data(alert['_source']) for alert in result.body['hits']['hits'] ]

    def __delete__(self):
        self.es.close()
