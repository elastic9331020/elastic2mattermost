from mattermostdriver import Driver
import logging
logger = logging.getLogger('MattermostConnector')
from datetime import datetime, timedelta

class MattermostConnector:

    def __init__(self, scheme, url, port, token, debug = False):
        '''
        Initialise la connexion avec Mattermost
        '''
        self.driver = Driver({
            'url': url,
            'token': token,
            'scheme': scheme,
            'port': port,
            'basepath': '/api/v4',
            # 'verify': False,  # Or /path/to/file.pem
            'auth': None,
            'timeout': 30,
            'request_timeout': None,
            'keepalive': False,
            'keepalive_delay': 5,
            'websocket_kw_args': None,
            'debug': debug
        })

        self.driver.login()
        self.elasticbot_channel_id = self.driver.channels.get_channel_by_name_and_team_name('forgecloud', 'ElasticBot')['id']
        self.alertid2mmtid = {}


        # Update
        page = 0
        posts = {"init":"blabla"}
        while len(posts) > 0:
            posts = self.driver.posts.get_posts_for_channel(self.elasticbot_channel_id, params={"page": page})['posts']
            for post_id, post in posts.items():
                print(post)
                # Rage cleanup mode
                if not 'deleteBy' in post['props'] and datetime.fromtimestamp(post['create_at']/1000) < datetime.now() - timedelta(days=1):
                    try:
                        self.driver.posts.delete_post(post_id=post['id'])
                    except:
                        print('-----------------------------------------')
                        print("delete post KO" + post['id'])
                        print('-----------------------------------------')
                elif 'props' in post and 'id' in post['props'] and post['props'].get('from_bot', 'false') == 'true':
                    logger.debug('unpine post %s', post_id)
                    self.driver.posts.unpin_post_to_channel(post_id=post_id)
                    if post.get('root_id',"") == "": # On ne recupere que les message racine
                        self.add_alert2mattermost_mapping(alert_id=post['props']['id'], post_id=post_id)
            page += 1

        logger.info('Found %s message, storing in cache..', len(self.alertid2mmtid))

    def add_alert2mattermost_mapping(self, alert_id, post_id):
        '''
        Mapping permettant de trouver a partir de l'id de l'alert le message existant dans mattermost
        '''
        self.alertid2mmtid[alert_id] = post_id

    def remove_alert2mattermost_mapping(self, alert_id):
        '''
        Supprime l'alerte du cache mapping
        Les alertents non traitées seront "unpin"
        '''
        del self.alertid2mmtid[alert_id]

    def notify_alert(self, alert):
        '''
        Met a jour mattermost avec une nouvelle alert
        '''
        post_id = -1
        logger.debug("Verification de la presence de l'id %s dans le CACHE : %s",alert.id, str(self.alertid2mmtid))
        if alert.id in self.alertid2mmtid:
            post_id = self.alertid2mmtid[alert.id]
            last_post = self.driver.posts.get_post(post_id=post_id)
            new_message = str(alert)
            if last_post['message'] != new_message:
                logger.info('notify_alert update Alert message  %s, post_id %s',alert.id, post_id)
                new_post = self.driver.posts.update_post(
                    post_id=post_id,
                    options={
                    'id': post_id,
                    'channel_id': self.elasticbot_channel_id,
                    'message': new_message,
                    'props':{
                        "id": alert.id,
                        "@timestamp": alert.timestamp,
                        'clos': not alert.is_open
                    }
                })
                logger.debug("MAJ message root_id %s avec message precedent", str(new_post['id']))
                self.driver.posts.create_post(options={
                    'channel_id': self.elasticbot_channel_id,
                    'root_id': new_post['id'],
                    'message': last_post['message'],
                    'props':{
                        "id": alert.id,
                        "@timestamp": alert.timestamp,
                        'clos': not alert.is_open
                    }
                })
            else:
                logger.info('notify_alert Alert message  %s, post_id %s : Pas de modification',alert.id, post_id)



            self.remove_alert2mattermost_mapping(alert.id)
        else:
            logger.info('notify_alert create Alert message  %s',alert.id)
            resp = self.driver.posts.create_post(options={
                'channel_id': self.elasticbot_channel_id,
                'message': str(alert),
                'props':{
                    "id": alert.id,
                    "@timestamp": alert.timestamp,
                    'clos': not alert.is_open
                }
            })
            post_id = resp['id']
        self.driver.posts.pin_post_to_channel(post_id=post_id)

    def __delete__(self):
        for post, post_id in self.alertid2mmtid.items():
            if post.get('props', {}).get('clos', False) is True:
                logger.info('delete close alert post %s', post_id)
                self.driver.posts.delete_post(post_id=post_id)

        self.driver.disconnect()
